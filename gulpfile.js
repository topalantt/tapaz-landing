'use strict';

const gulp = require('gulp');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

function lazyRequireTask(taskName, path, options) {
    options = options || {};
    options.taskName = taskName;
    gulp.task(taskName, function(callback) {
        let task = require(path).call(this, options);

        return task(callback);
    });
}

lazyRequireTask('css', './tasks/css', {
    src: 'frontend/sass/app.scss'
});

lazyRequireTask('uglifyJs', './tasks/uglifyJs', {
    src: 'frontend/js/*.js'
});

lazyRequireTask('assets', './tasks/assets', {
    src: 'frontend/assets/**'
});

lazyRequireTask('clean', './tasks/clean', {
    dst: 'public'
});

lazyRequireTask('serve', './tasks/serve', {
    dst: 'public'
});

lazyRequireTask('handlebars', './tasks/handlebars', {
    src: 'frontend/templates/pages/*.hbs',
    structure: 'frontend/templates/structure',
    sections: 'frontend/templates/sections',
    dst: 'public'
});

gulp.task('watch', function() {

    gulp.watch('frontend/sass/*scss', gulp.series('css')).on('change', function (event) {
        if (event.type === 'deleted') {
            delete cache.caches['css'][event.path];
            remember.forget('css', event.path);
        }
    });

    gulp.watch('frontend/js/*.js', gulp.series('uglifyJs'));
    gulp.watch('frontend/assets/**/*.*', gulp.series('assets'));
    gulp.watch('frontend/templates/**/*.{hbs}', gulp.series('handlebars'));
});

gulp.task('build', gulp.series('clean', gulp.parallel('css', 'uglifyJs', 'assets', 'handlebars')));

gulp.task('dev',
    gulp.series('build', gulp.parallel('watch', 'serve'))
);


